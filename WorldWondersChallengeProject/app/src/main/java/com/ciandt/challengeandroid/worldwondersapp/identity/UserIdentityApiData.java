package com.ciandt.challengeandroid.worldwondersapp.identity;

import com.ciandt.challengeandroid.worldwondersapp.api.ApiEntity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Tiago on 15/08/2015.
 */
public class UserIdentityApiData implements ApiEntity{

    private String token_auth;

    private String nome;

    private boolean habilitado;

    public UserIdentityApiData(JSONObject json) throws JSONException{
        this.fromJson(json);
    }

    @Override
    public void fromJson(JSONObject json) throws JSONException {

        this.token_auth = json.getString("token_auth");
        this.nome = json.getString("nome");
        this.habilitado = json.getBoolean("habilitado");


    }


    public String getName()
    {
        return this.nome;
    }
}
