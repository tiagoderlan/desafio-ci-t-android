package com.ciandt.challengeandroid.worldwondersapp.utils;

import android.text.TextUtils;

/**
 * Created by Tiago on 16/08/2015.
 */
public class ValidationUtils {

    public final static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }
}
