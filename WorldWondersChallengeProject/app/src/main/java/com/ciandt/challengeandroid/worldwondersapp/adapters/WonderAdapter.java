package com.ciandt.challengeandroid.worldwondersapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.collections.WonderCollection;
import com.ciandt.challengeandroid.worldwondersapp.models.Wonder;

/**
 * Created by Tiago on 15/08/2015.
 */
public class WonderAdapter extends ArrayAdapter<Wonder>
{
    private LayoutInflater inflater;

    public WonderAdapter(Context context, WonderCollection items) {
        super(context, R.layout.wonder_list_item);

        inflater = LayoutInflater.from(context);

        this.addAll(items);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null)
            convertView = inflater.inflate(R.layout.wonder_list_item, null);


        TextView name = (TextView)convertView.findViewById(R.id.wonder_list_item_name);
        TextView country = (TextView)convertView.findViewById(R.id.wonder_list_item_country);
        TextView description = (TextView)convertView.findViewById(R.id.wonder_list_item_description);

        Wonder item = getItem(position);


        name.setText(item.getName());
        country.setText(item.getCountry());
        description.setText(item.getDescription());

        return convertView;

    }
}
