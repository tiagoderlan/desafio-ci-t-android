package com.ciandt.challengeandroid.worldwondersapp.collections;

import com.ciandt.challengeandroid.worldwondersapp.api.ApiCollection;
import com.ciandt.challengeandroid.worldwondersapp.models.Wonder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Tiago on 15/08/2015.
 */
public class WonderCollection extends ArrayList<Wonder> implements ApiCollection
{

    public WonderCollection(String raw) throws JSONException
    {
        this.fromJsonArray(raw);
    }

    @Override
    public void fromJsonArray(String raw) throws JSONException {

        JSONObject json = new JSONObject(raw);

        JSONArray array = json.getJSONArray("data");

        int size = array.length();

        for (int i = 0; i < size; i++) {
            this.add(new Wonder(array.getJSONObject(i)));
        }


    }

    public Wonder[] toArray()
    {
        Wonder[] result = new Wonder[this.size()];

        return super.toArray(result);
    }
}
