package com.ciandt.challengeandroid.worldwondersapp.api;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Tiago on 15/08/2015.
 */
public interface ApiEntity {

    void fromJson(JSONObject json) throws JSONException;

}
