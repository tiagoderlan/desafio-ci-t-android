package com.ciandt.challengeandroid.worldwondersapp.activities;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.api.ApiUtils;
import com.ciandt.challengeandroid.worldwondersapp.enums.LoginResult;
import com.ciandt.challengeandroid.worldwondersapp.identity.IdentityApiData;
import com.ciandt.challengeandroid.worldwondersapp.identity.IdentityStore;
import com.ciandt.challengeandroid.worldwondersapp.utils.NetworkUtils;
import com.ciandt.challengeandroid.worldwondersapp.utils.ValidationUtils;

/**
 * Created by Tiago on 16/08/2015.
 */
public class LoginActivity extends AppCompatActivity implements View.OnClickListener
{
    private EditText emailfield;

    private EditText passfield;

    private Button loginbutton;

    private Dialog busy_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        emailfield = (EditText)findViewById(R.id.activity_login_email_field);
        passfield = (EditText)findViewById(R.id.activity_login_pass_field);
        loginbutton = (Button)findViewById(R.id.activity_login_enter_button);
        loginbutton.setOnClickListener(this);

        /*Em caso de haver identidade anterior*/
        if (IdentityStore.getInstance(this).isValid())
        {

            String email = IdentityStore.getInstance(this).getEmail();
            String pass = IdentityStore.getInstance(this).getPass();
            showWonderListActivity(email, pass);
        }
    }


    private void login(String email, String pass)
    {
        AsyncTask<String, LoginResult, IdentityApiData> task = new AsyncTask<String, LoginResult, IdentityApiData>() {

            private String email;
            private String pass;

            @Override
            protected void onPreExecute()
            {
                createProgressAndShow();

            }

            @Override
            protected void onProgressUpdate(LoginResult... values) {
                super.onProgressUpdate(values);

                switch (values[0])
                {
                    case Failed:
                        Toast.makeText(LoginActivity.this, "Falha ao entrar, verifique sua senha e tente novamente", Toast.LENGTH_LONG).show();
                        break;
                    case Conectivity:
                        Toast.makeText(LoginActivity.this, "Falha ao acessar a internet, verifique sua conexao e tente novamente", Toast.LENGTH_LONG).show();
                        break;
                }
            }

            @Override
            protected IdentityApiData doInBackground(String... strings)
            {
                this.email = strings[0];
                this.pass = strings[1];

                if(NetworkUtils.isOnline(LoginActivity.this))
                {
                    IdentityApiData data = ApiUtils.login(this.email, this.pass);

                    if(data == null)
                        publishProgress(LoginResult.Failed);
                    else
                        return data;
                }
                else
                    publishProgress(LoginResult.Failed);

                return null;
            }

            @Override
            protected void onPostExecute(IdentityApiData data) {
                super.onPostExecute(data);

                busy_dialog.dismiss();

                if(data != null)
                {


                    IdentityStore.getInstance(LoginActivity.this).putData(data.getDados().getName(),email, pass);

                    showWonderListActivity(email, pass);

                }

            }
        };

        task.execute(email, pass);
    }

    private void createProgressAndShow()
    {
        LayoutInflater inflater = LayoutInflater.from(this);

        busy_dialog = new Dialog(this, R.style.AppTheme);

        busy_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        busy_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#55000000")));

        View view = inflater.inflate(R.layout.busy_dialog, null);

        ImageView image = (ImageView)view.findViewById(R.id.busy_dialog_image);

        RotateAnimation rotateAnimation = new RotateAnimation(360, 0,   Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        rotateAnimation.setDuration(1200);
        image.startAnimation(rotateAnimation);

        TextView textmessage = (TextView)view.findViewById(R.id.busy_dialog_text);

        textmessage.setText("Verificando suas credenciais...");

        busy_dialog.setContentView(view);

        busy_dialog.setCancelable(false);

        busy_dialog.show();

    }

    private void showWonderListActivity(String email, String pass)
    {
        Intent intent = new Intent(this, WonderListActivity.class);

        this.startActivity(intent);

    }


    @Override
    public void onClick(View view)
    {
        String email = emailfield.getText().toString();
        String pass = passfield.getText().toString();
        boolean error = false;

        if(email.isEmpty())
        {
            emailfield.setError("Informe seu email!");
            error = true;
        }
        else if (!ValidationUtils.isValidEmail(email))
        {
            emailfield.setError("Email inválido!");
            error = true;
        }

        if(pass.isEmpty())
        {
            passfield.setError("Informe sua senha!");
            error = true;
        }

        if(!error)
            login(email, pass);
    }
}
