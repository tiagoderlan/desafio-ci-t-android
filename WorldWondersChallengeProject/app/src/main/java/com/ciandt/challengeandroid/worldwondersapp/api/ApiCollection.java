package com.ciandt.challengeandroid.worldwondersapp.api;

import org.json.JSONException;

/**
 * Created by Tiago on 15/08/2015.
 */
public interface ApiCollection {

    void fromJsonArray(String raw) throws JSONException;
}
