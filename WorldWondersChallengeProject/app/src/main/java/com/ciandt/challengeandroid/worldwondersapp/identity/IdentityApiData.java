package com.ciandt.challengeandroid.worldwondersapp.identity;

import com.ciandt.challengeandroid.worldwondersapp.api.ApiEntity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Tiago on 15/08/2015.
 */
public class IdentityApiData implements ApiEntity{

    private int codigo;

    private String mensagem;

    private UserIdentityApiData dados;

    public IdentityApiData(String raw) throws JSONException
    {
        JSONObject json = new JSONObject(raw);

        this.fromJson(json);
    }


    @Override
    public void fromJson(JSONObject json) throws JSONException {
        this.codigo = json.getInt("codigo");
        this.mensagem = json.getString("mensagem");
        this.dados = new UserIdentityApiData(json.getJSONObject("dados"));

    }

    public UserIdentityApiData getDados()
    {
        return this.dados;
    }
}
