package com.ciandt.challengeandroid.worldwondersapp.activities;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ciandt.challengeandroid.worldwondersapp.R;
import com.ciandt.challengeandroid.worldwondersapp.adapters.WonderAdapter;
import com.ciandt.challengeandroid.worldwondersapp.api.ApiUtils;
import com.ciandt.challengeandroid.worldwondersapp.collections.WonderCollection;
import com.ciandt.challengeandroid.worldwondersapp.enums.GetListResult;
import com.ciandt.challengeandroid.worldwondersapp.identity.IdentityStore;


public class WonderListActivity extends AppCompatActivity
{
    private Dialog busy_dialog;

    private ListView list_wonders;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_wonders);

        list_wonders = (ListView)findViewById(R.id.activity_wonders_list);

        AsyncTask<Void, GetListResult, WonderCollection> task = new AsyncTask<Void, GetListResult, WonderCollection>() {

            @Override
            protected void onPreExecute()
            {
                createProgressAndShow();

            }

            @Override
            protected void onProgressUpdate(GetListResult... values) {
                super.onProgressUpdate(values);

                if(values[0] == GetListResult.Failed)
                    Toast.makeText(WonderListActivity.this, "Falha ao recuperar maravilhas", Toast.LENGTH_LONG).show();
            }

            @Override
            protected WonderCollection doInBackground(Void... strings)
            {
                WonderCollection items = ApiUtils.getWonders();

                if(items == null) {
                    publishProgress(GetListResult.Failed);
                    return null;
                }
                else
                {
                    return items;
                }
            }

            @Override
            protected void onPostExecute(WonderCollection wonders) {
                super.onPostExecute(wonders);

                if(wonders != null)
                    list_wonders.setAdapter(new WonderAdapter(WonderListActivity.this, wonders));

                busy_dialog.dismiss();

                Toast.makeText(WonderListActivity.this, "Olá " + IdentityStore.getInstance(WonderListActivity.this).getName(), Toast.LENGTH_LONG).show();
            }
        };

        task.execute();
    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }

    private void createProgressAndShow()
    {
        LayoutInflater inflater = LayoutInflater.from(this);

        busy_dialog = new Dialog(this, R.style.AppTheme);

        busy_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        busy_dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#55000000")));

        View view = inflater.inflate(R.layout.busy_dialog, null);

        ImageView image = (ImageView)view.findViewById(R.id.busy_dialog_image);

        RotateAnimation rotateAnimation = new RotateAnimation(360, 0,   Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);

        rotateAnimation.setInterpolator(new LinearInterpolator());
        rotateAnimation.setRepeatCount(Animation.INFINITE);
        rotateAnimation.setDuration(1200);
        image.startAnimation(rotateAnimation);

        TextView textmessage = (TextView)view.findViewById(R.id.busy_dialog_text);

        textmessage.setText("Recuperando maravilhas do mundo...");

        busy_dialog.setContentView(view);

        busy_dialog.setCancelable(false);

        busy_dialog.show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logoff)
        {
            IdentityStore.getInstance(this).clear();

            finish();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
