package com.ciandt.challengeandroid.worldwondersapp.models;

import com.ciandt.challengeandroid.worldwondersapp.api.ApiEntity;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Tiago on 15/08/2015.
 */
public class Wonder implements ApiEntity
{
    private int id;

    private String name;

    private String country;

    private String description;

    private String image_url;

    public Wonder()
    {

    }

    public Wonder(JSONObject json) throws JSONException
    {
        this.fromJson(json);
    }

    @java.lang.Override
    public void fromJson(JSONObject json) throws JSONException
    {
        this.id = json.getInt("id");
        this.setName(json.getString("name"));
        this.setCountry(json.getString("country"));
        this.setDescription(json.getString("description"));
        this.image_url = json.getString("image_url");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
