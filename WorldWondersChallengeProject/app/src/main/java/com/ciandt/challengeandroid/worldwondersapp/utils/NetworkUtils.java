package com.ciandt.challengeandroid.worldwondersapp.utils;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * Created by Tiago on 16/08/2015.
 */
public class NetworkUtils {

    public static boolean isOnline(Context context)
    {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
