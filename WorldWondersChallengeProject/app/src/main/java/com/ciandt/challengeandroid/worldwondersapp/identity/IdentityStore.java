package com.ciandt.challengeandroid.worldwondersapp.identity;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Created by Tiago on 15/08/2015.
 */
public class IdentityStore{
    public static final String EMAIL_PREFERENCE_KEY = "com.ciandt.challengeandroid.worldwondersapp.identity-email-pref-key";

    public static final String PASS_PREFERENCE_KEY = "com.ciandt.challengeandroid.worldwondersapp.identity-pass-pref-key";

    public static final String NAME_PREFERENCE_KEY = "com.ciandt.challengeandroid.worldwondersapp.identity-name-pref-key";

    public static final String PREFERENCE_KEY = "com.ciandt.challengeandroid.worldwondersapp.identity-app-preference-key";

    private String name;

    private String email;

    private String pass;

    private static IdentityStore instance;

    private Context context;

    public static IdentityStore getInstance(Context context)
    {
        if(instance == null)
            instance = new IdentityStore(context);

        return instance;
    }

    private IdentityStore(Context context)
    {
        this.context = context;

        loadPreferences();
    }

    public boolean isValid()
    {
        return this.email != null && this.pass != null && this.name != null;
    }


    private void savePreferences()
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE);

        Editor editor = prefs.edit();

        editor.putString(NAME_PREFERENCE_KEY, this.name);
        editor.putString(EMAIL_PREFERENCE_KEY, this.email);
        editor.putString(PASS_PREFERENCE_KEY, this.pass);

        editor.commit();
    }

    private void loadPreferences()
    {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE);

        this.email = prefs.getString(EMAIL_PREFERENCE_KEY, null);
        this.pass = prefs.getString(PASS_PREFERENCE_KEY, null);
        this.name = prefs.getString(NAME_PREFERENCE_KEY, null);
    }

    public void putData(String name, String email, String pass)
    {
        this.name = name;
        this.email = email;
        this.pass = pass;

        savePreferences();
    }

    public String getName()
    {
        return this.name;
    }

    public String getEmail()
    {
        return this.email;
    }

    public String getPass()
    {
        return this.pass;
    }

    public void clear() {
        SharedPreferences prefs = context.getSharedPreferences(PREFERENCE_KEY, Context.MODE_PRIVATE);

        Editor editor = prefs.edit();

        editor.remove(EMAIL_PREFERENCE_KEY);
        editor.remove(PASS_PREFERENCE_KEY);
        editor.remove(NAME_PREFERENCE_KEY);

        this.email = null;
        this.pass = null;

        editor.commit();
    }
}