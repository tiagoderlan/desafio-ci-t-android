package com.ciandt.challengeandroid.worldwondersapp.api;

import com.ciandt.challengeandroid.worldwondersapp.collections.WonderCollection;
import com.ciandt.challengeandroid.worldwondersapp.identity.IdentityApiData;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.MessageFormat;
import java.util.Scanner;

/**
 * Created by Tiago on 15/08/2015.
 */
public class ApiUtils {

    /*public static boolean login(String email, String pass){

        String url = "http://private-anon-1a3cbff5a-worldwondersapi.apiary-mock.com";

        HttpURLConnection urlConnection = null;

        try
        {
            URL urlToRequest = new URL(url);
            urlConnection = (HttpURLConnection)urlToRequest.openConnection();
            urlConnection.setConnectTimeout(500000);
            urlConnection.setReadTimeout(500000);

            urlConnection.addRequestProperty("Content-Type","application/json");
            urlConnection.addRequestProperty("Accept", "text/plain");

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_OK)
            {

            }
            else
            {

            }

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            return (getResponseText(in));

        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        return null;
    }*/


    public static WonderCollection getWonders()
    {
        String url = "http://private-anon-1a3cbff5a-worldwondersapi.apiary-mock.com/api/v1/worldwonders";

        HttpURLConnection urlConnection = null;

        try
        {
            URL urlToRequest = new URL(url);
            urlConnection = (HttpURLConnection)urlToRequest.openConnection();
            urlConnection.setConnectTimeout(500000);
            urlConnection.setReadTimeout(500000);

            urlConnection.addRequestProperty("Content-Type","application/json");

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String rawjson = getStringFromStream(in);

                return new WonderCollection(rawjson);
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        return null;
    }


    public static IdentityApiData login(String email, String pass)
    {
        String url = "http://private-anon-1a3cbff5a-worldwondersapi.apiary-mock.com/api/v1/login?email={0}&senha={1}";

        HttpURLConnection urlConnection = null;

        try
        {
            URL urlToRequest = new URL(MessageFormat.format(url, email, pass));
            urlConnection = (HttpURLConnection)urlToRequest.openConnection();
            urlConnection.setConnectTimeout(500000);
            urlConnection.setReadTimeout(500000);

            urlConnection.addRequestProperty("Content-Type","application/json");

            int statusCode = urlConnection.getResponseCode();

            if (statusCode == HttpURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());

                String rawjson = getStringFromStream(in);

                return new IdentityApiData(rawjson);
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
        finally
        {
            if (urlConnection != null)
                urlConnection.disconnect();
        }

        return null;
    }


    private static String getStringFromStream(InputStream in)
    {
        return new Scanner(in).useDelimiter("\\A").next();
    }

}
